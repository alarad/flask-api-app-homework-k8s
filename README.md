# flask-api-app-homework-k8s

Simple k8s flask app managing users in a mysql database.
In this example, the app will run in a local k3d kubernetes cluster.

## Pre-requisites

In order to run the app in k3d, you will installed on your linux system **Docker v20.10.5 (runc >= v1.0.0-rc93)**.

An easy way to install docker, on almost all linux distros, is using the snap command ```sudo snap install docker```.

**We consider, for this exercice that docker is already pre-installed on your system, as we can not know in advance which distribution of linux will be used**.

## Run the app on a local k3d cluster

For this exercice, for practical reasons, we decided to not use a container registry with the k3d cluster.
We'll import the images directly in the cluster using k3d import commands.

1- Install k3d (here version 5.5.1):
```bash
sudo curl -s https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | TAG=v5.5.1 bash
```
2- Install kubectl (here version 1.26.4, compatible with k3d 5.5.1) :
```bash
sudo curl -LO https://dl.k8s.io/release/v1.26.4/bin/linux/amd64/kubectl
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```
3- from the root of the current project, build the flask app docker image :
```bash
docker build . -t flask-userapi
```
4- Pull from dockerhub the mysql image needed for the app : 
```bash
docker pull mysql:8.0.33
```
5- Create a k3d cluster : 
```bash
k3d cluster create myflaskcluster
```
6- Confirm you can reach it correctly : 
```bash
kubectl get nodes
```
7- Create the k8s secret that will be used by the deployments : 
```bash
kubectl create secret generic mysql-secret --from-literal=db-root-password=P@ssW0RDMySqL
```
8- Import boths container images in the cluster :
```bash
k3d image import flask-userapi:latest -c myflaskcluster
k3d image import mysql:8.0.33 -c myflaskcluster
```
9- Apply the flask app deploy yaml file in the cluster :
```bash
kubectl apply -f flaskapp.yaml
```
10- Confirm all pods are running, usually takes less than a minute :
```bash
kubectl get pods
```

The pod mysql-create-db-table, being a job, should display "completed" in it status.

If not running, you can try delete and re-apply :
```bash
kubectl delete -f flaskapp.yaml
kubectl apply -f flaskapp.yaml
```

11- Access the app locally using port-forward :
```bash
kubectl port-forward service/flask-userapi-service 5000:5000
```

12- Run the following curl commands to confirm the API works as expected : 
```bash
curl http://localhost:5000/users
curl -X POST -H "Content-Type: application/json" -d '{"name": "John Doe", "email": "johndoe@example.com", "pwd": "secretpassword"}' http://localhost:5000/create
curl http://localhost:5000/users
curl http://localhost:5000/user/1
curl -X POST -H "Content-Type: application/json" -d '{"user_id": 1,"name": "John smith", "email": "johnsmith@example.com", "pwd": "newsecretpassword"}' http://localhost:5000/update
curl http://localhost:5000/user/1
curl http://localhost:5000/delete/1
curl http://localhost:5000/users
```

You should see the list of users (empty at first), then the user johndoe created, then modified to john smith, then deleted, to end with an empty list of users again.

## Next steps

CICD would be next in line. Brief ideas are specified in cicd-ideas.txt