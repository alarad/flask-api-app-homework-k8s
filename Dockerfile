FROM python:3.6
COPY ./ ./
RUN pip install -r app/requirements.txt
CMD FLASK_APP=app/userapi.py flask run --host=0.0.0.0